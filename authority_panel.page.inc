<?php

/**
 * @file
 * Contains authority_panel.page.inc.
 *
 * Page callback for Authority Panel entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Authority Panel templates.
 *
 * Default template: authority_panel.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_authority_panel(array &$variables) {
  // Fetch AuthorityPanel Entity Object.
  $authority_panel = $variables['elements']['#authority_panel'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

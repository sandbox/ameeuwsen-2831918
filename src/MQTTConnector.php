<?php

namespace Drupal\drouse;

class MQTTConnector {

  /**
   * MQTT instance
   *
   * @var
   */
  public $mqtt;

  private $user = 'admin';

  private $pass = 'admin';

  /**
   * MQTTConnector constructor.
   */
  public function __construct() {
    $this->mqtt = new phpMQTT('172.16.91.222', '1883', "ClientID".rand());
  }

  /**
   * MQTT publish
   */
  public function publish($topic, $message, $qos) {
    //MQTT client id to use for the device. "" will generate a client id automatically
    $mqtt = new phpMQTT('172.16.91.222', '1883', "ClientID".rand());

    if ($mqtt->connect(true,NULL,$this->user,$this->pass)) {
      $mqtt->publish($topic,$message, $qos);
      $mqtt->close();
    }else{
      echo "Fail or time out<br />";
    }
  }

  /**
   * MQTT subscribe
   */
  public function subscribe($topic) {
    if(!$this->mqtt->connect(true,NULL,$this->user,$this->pass)){
      exit(1);
    }

    //currently subscribed topics
    $topics[$topic] = array("qos"=>2, "function"=>"procmsg");
    $this->mqtt->subscribe($topics,0);

    while($this->mqtt->proc()){
    }

    $this->mqtt->close();
  }

}
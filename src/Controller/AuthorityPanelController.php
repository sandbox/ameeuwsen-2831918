<?php

namespace Drupal\drouse\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\drouse\Entity\AuthorityPanelInterface;

/**
 * Class AuthorityPanelController.
 *
 *  Returns responses for Authority Panel routes.
 *
 * @package Drupal\drouse\Controller
 */
class AuthorityPanelController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Authority Panel  revision.
   *
   * @param int $authority_panel_revision
   *   The Authority Panel  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($authority_panel_revision) {
    $authority_panel = $this->entityManager()->getStorage('authority_panel')->loadRevision($authority_panel_revision);
    $view_builder = $this->entityManager()->getViewBuilder('authority_panel');

    return $view_builder->view($authority_panel);
  }

  /**
   * Page title callback for a Authority Panel  revision.
   *
   * @param int $authority_panel_revision
   *   The Authority Panel  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($authority_panel_revision) {
    $authority_panel = $this->entityManager()->getStorage('authority_panel')->loadRevision($authority_panel_revision);
    return $this->t('Revision of %title from %date', array('%title' => $authority_panel->label(), '%date' => format_date($authority_panel->getRevisionCreationTime())));
  }

  /**
   * Generates an overview table of older revisions of a Authority Panel .
   *
   * @param \Drupal\drouse\Entity\AuthorityPanelInterface $authority_panel
   *   A Authority Panel  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(AuthorityPanelInterface $authority_panel) {
    $account = $this->currentUser();
    $langcode = $authority_panel->language()->getId();
    $langname = $authority_panel->language()->getName();
    $languages = $authority_panel->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $authority_panel_storage = $this->entityManager()->getStorage('authority_panel');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $authority_panel->label()]) : $this->t('Revisions for %title', ['%title' => $authority_panel->label()]);
    $header = array($this->t('Revision'), $this->t('Operations'));

    $revert_permission = (($account->hasPermission("revert all authority panel revisions") || $account->hasPermission('administer authority panel entities')));
    $delete_permission = (($account->hasPermission("delete all authority panel revisions") || $account->hasPermission('administer authority panel entities')));

    $rows = array();

    $vids = $authority_panel_storage->revisionIds($authority_panel);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\drouse\AuthorityPanelInterface $revision */
      $revision = $authority_panel_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionAuthor(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->revision_timestamp->value, 'short');
        if ($vid != $authority_panel->getRevisionId()) {
          $link = $this->l($date, new Url('entity.authority_panel.revision', ['authority_panel' => $authority_panel->id(), 'authority_panel_revision' => $vid]));
        }
        else {
          $link = $authority_panel->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->revision_log_message->value, '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute('authority_panel.revision_revert_confirm', ['authority_panel' => $authority_panel->id(), 'authority_panel_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('authority_panel.revision_delete_confirm', ['authority_panel' => $authority_panel->id(), 'authority_panel_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['authority_panel_revisions_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    );

    return $build;
  }

}

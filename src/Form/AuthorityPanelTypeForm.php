<?php

namespace Drupal\drouse\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AuthorityPanelTypeForm.
 *
 * @package Drupal\drouse\Form
 */
class AuthorityPanelTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $authority_panel_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $authority_panel_type->label(),
      '#description' => $this->t("Label for the Authority Panel type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $authority_panel_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\drouse\Entity\AuthorityPanelType::load',
      ],
      '#disabled' => !$authority_panel_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $authority_panel_type = $this->entity;
    $status = $authority_panel_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Authority Panel type.', [
          '%label' => $authority_panel_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Authority Panel type.', [
          '%label' => $authority_panel_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($authority_panel_type->urlInfo('collection'));
  }

}

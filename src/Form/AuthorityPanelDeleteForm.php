<?php

namespace Drupal\drouse\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Authority Panel entities.
 *
 * @ingroup drouse
 */
class AuthorityPanelDeleteForm extends ContentEntityDeleteForm {


}

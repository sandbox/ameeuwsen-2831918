<?php

namespace Drupal\drouse;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Authority Panel entity.
 *
 * @see \Drupal\drouse\Entity\AuthorityPanel.
 */
class AuthorityPanelAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\drouse\Entity\AuthorityPanelInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished authority panel entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published authority panel entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit authority panel entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete authority panel entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add authority panel entities');
  }

}

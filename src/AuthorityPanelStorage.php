<?php

namespace Drupal\drouse;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\drouse\Entity\AuthorityPanelInterface;

/**
 * Defines the storage handler class for Authority Panel entities.
 *
 * This extends the base storage class, adding required special handling for
 * Authority Panel entities.
 *
 * @ingroup drouse
 */
class AuthorityPanelStorage extends SqlContentEntityStorage implements AuthorityPanelStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(AuthorityPanelInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {authority_panel_revision} WHERE id=:id ORDER BY vid',
      array(':id' => $entity->id())
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {authority_panel_field_revision} WHERE uid = :uid ORDER BY vid',
      array(':uid' => $account->id())
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(AuthorityPanelInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {authority_panel_field_revision} WHERE id = :id AND default_langcode = 1', array(':id' => $entity->id()))
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('authority_panel_revision')
      ->fields(array('langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED))
      ->condition('langcode', $language->getId())
      ->execute();
  }

}

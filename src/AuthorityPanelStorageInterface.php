<?php

namespace Drupal\drouse;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\drouse\Entity\AuthorityPanelInterface;

/**
 * Defines the storage handler class for Authority Panel entities.
 *
 * This extends the base storage class, adding required special handling for
 * Authority Panel entities.
 *
 * @ingroup drouse
 */
interface AuthorityPanelStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Authority Panel revision IDs for a specific Authority Panel.
   *
   * @param \Drupal\drouse\Entity\AuthorityPanelInterface $entity
   *   The Authority Panel entity.
   *
   * @return int[]
   *   Authority Panel revision IDs (in ascending order).
   */
  public function revisionIds(AuthorityPanelInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Authority Panel author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Authority Panel revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\drouse\Entity\AuthorityPanelInterface $entity
   *   The Authority Panel entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(AuthorityPanelInterface $entity);

  /**
   * Unsets the language for all Authority Panel with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}

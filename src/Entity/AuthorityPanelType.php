<?php

namespace Drupal\drouse\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Authority Panel type entity.
 *
 * @ConfigEntityType(
 *   id = "authority_panel_type",
 *   label = @Translation("Authority Panel type"),
 *   handlers = {
 *     "list_builder" = "Drupal\drouse\AuthorityPanelTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\drouse\Form\AuthorityPanelTypeForm",
 *       "edit" = "Drupal\drouse\Form\AuthorityPanelTypeForm",
 *       "delete" = "Drupal\drouse\Form\AuthorityPanelTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\drouse\AuthorityPanelTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "authority_panel_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "authority_panel",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/authority_panel_type/{authority_panel_type}",
 *     "add-form" = "/admin/structure/authority_panel_type/add",
 *     "edit-form" = "/admin/structure/authority_panel_type/{authority_panel_type}/edit",
 *     "delete-form" = "/admin/structure/authority_panel_type/{authority_panel_type}/delete",
 *     "collection" = "/admin/structure/authority_panel_type"
 *   }
 * )
 */
class AuthorityPanelType extends ConfigEntityBundleBase implements AuthorityPanelTypeInterface {

  /**
   * The Authority Panel type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Authority Panel type label.
   *
   * @var string
   */
  protected $label;

}

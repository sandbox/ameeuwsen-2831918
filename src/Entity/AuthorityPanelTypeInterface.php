<?php

namespace Drupal\drouse\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Authority Panel type entities.
 */
interface AuthorityPanelTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}

<?php

namespace Drupal\drouse\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Authority Panel entities.
 */
class AuthorityPanelViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['authority_panel']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Authority Panel'),
      'help' => $this->t('The Authority Panel ID.'),
    );

    return $data;
  }

}

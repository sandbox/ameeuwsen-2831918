<?php

namespace Drupal\drouse\Entity;

use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Authority Panel entities.
 *
 * @ingroup drouse
 */
interface AuthorityPanelInterface extends RevisionableInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Authority Panel type.
   *
   * @return string
   *   The Authority Panel type.
   */
  public function getType();

  /**
   * Gets the Authority Panel name.
   *
   * @return string
   *   Name of the Authority Panel.
   */
  public function getName();

  /**
   * Sets the Authority Panel name.
   *
   * @param string $name
   *   The Authority Panel name.
   *
   * @return \Drupal\drouse\Entity\AuthorityPanelInterface
   *   The called Authority Panel entity.
   */
  public function setName($name);

  /**
   * Gets the Authority Panel creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Authority Panel.
   */
  public function getCreatedTime();

  /**
   * Sets the Authority Panel creation timestamp.
   *
   * @param int $timestamp
   *   The Authority Panel creation timestamp.
   *
   * @return \Drupal\drouse\Entity\AuthorityPanelInterface
   *   The called Authority Panel entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Authority Panel published status indicator.
   *
   * Unpublished Authority Panel are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Authority Panel is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Authority Panel.
   *
   * @param bool $published
   *   TRUE to set this Authority Panel to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\drouse\Entity\AuthorityPanelInterface
   *   The called Authority Panel entity.
   */
  public function setPublished($published);

  /**
   * Gets the Authority Panel revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Authority Panel revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\drouse\Entity\AuthorityPanelInterface
   *   The called Authority Panel entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Authority Panel revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionAuthor();

  /**
   * Sets the Authority Panel revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\drouse\Entity\AuthorityPanelInterface
   *   The called Authority Panel entity.
   */
  public function setRevisionAuthorId($uid);

}
